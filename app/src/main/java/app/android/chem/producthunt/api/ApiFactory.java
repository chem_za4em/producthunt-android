package app.android.chem.producthunt.api;

import android.support.annotation.NonNull;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import app.android.chem.producthunt.BuildConfig;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by chem on 05.09.17.
 */

public class ApiFactory {

    private static OkHttpClient sClient;
    private static volatile ProductHuntService productHuntService;

    private ApiFactory() {
    }

    @NonNull
    public static ProductHuntService getReminderService(){
        ProductHuntService service = productHuntService;
        if (productHuntService == null){
            synchronized (ApiFactory.class){
                service = productHuntService;
                if (service == null)
                    service = productHuntService = buildRetrofit().create(ProductHuntService.class);
            }
        }
        return service;
    }

    @NonNull
    private static Retrofit buildRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }


    @NonNull
    private static OkHttpClient getClient() {
        OkHttpClient client = sClient;
        if (client == null) {
            synchronized (ApiFactory.class) {
                client = sClient;
                if (client == null) {
                    client = sClient = buildClient();
                }
            }
        }
        return client;
    }


    @NonNull
    private static OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(TokenInterceptor.create())
                .build();
    }

    public static void recreate() {
        sClient = null;
        sClient = getClient();
        productHuntService = buildRetrofit().create(ProductHuntService.class);
    }
}
