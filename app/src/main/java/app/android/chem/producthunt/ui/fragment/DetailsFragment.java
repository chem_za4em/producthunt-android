package app.android.chem.producthunt.ui.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import app.android.chem.producthunt.R;
import app.android.chem.producthunt.model.Post;
import app.android.chem.producthunt.presentation.presenter.DetailsPresenter;
import app.android.chem.producthunt.presentation.view.DetailsView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DetailsFragment extends Fragment implements DetailsView {

    public static final String TAG = "DetailsFragment";


    DetailsPresenter mDetailsPresenter;

    @BindView(R.id.product_name_detail)
    TextView productNameDetail;
    @BindView(R.id.screenshot)
    ImageView screenshot;
    @BindView(R.id.product_discription_detail)
    TextView productDiscriptionDetail;
    @BindView(R.id.up_votes_detail)
    TextView upVotesDetail;
    Unbinder unbinder;
    @BindView(R.id.detail_progress)
    ProgressBar detailProgress;

    private Post post;
    private ImageLoader imageLoader;

    public static DetailsFragment newInstance(Post post) {
        DetailsFragment fragment = new DetailsFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.post = post;
        return fragment;
    }

    @OnClick(R.id.get_it_button) void getIt(){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(post.getRedirectUrl()));
        startActivity(browserIntent);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        unbinder = ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.close, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.category_close) {
            getActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        imageLoader = ImageLoader.getInstance();
        productNameDetail.setText(post.getName());
        productDiscriptionDetail.setText(post.getTagline());
        upVotesDetail.setText(String.valueOf(post.getVotesCount()));
        imageLoader.loadImage(post.getScreenshotUrl().get300px(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                detailProgress.setVisibility(View.VISIBLE);
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                detailProgress.setVisibility(View.GONE);
                screenshot.setImageBitmap(loadedImage);
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
