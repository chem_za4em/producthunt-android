package app.android.chem.producthunt;

import android.app.Application;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.orhanobut.hawk.Hawk;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.rx.RealmObservableFactory;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

/**
 * Created by chem on 05.09.17.
 */

public class ProductHuntApp extends Application {

    private static Cicerone<Router> cicerone;
    static public DisplayImageOptions imgDisplayOptions = new DisplayImageOptions.Builder()
//            .showImageOnLoading(//)
            .cacheInMemory(true)
            .cacheOnDisk(true)
//         .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
//            .resetViewBeforeLoading(true)
            .build();



    @Override
    public void onCreate() {
        super.onCreate();
        initCicerone();
        initImageLoader();
        Hawk.init(this)
                .build();

        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .rxFactory(new RealmObservableFactory())
                .build();
        Realm.setDefaultConfiguration(configuration);
    }

    private void initImageLoader() {
        ImageLoader imageLoader = ImageLoader.getInstance();
        if (!imageLoader.isInited()) {
            // Ну прям со всем на всякий проверку сделал, чтобы точно не облажаться
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getBaseContext())
                    .memoryCacheSize(41943040)
                    .denyCacheImageMultipleSizesInMemory()
                    .diskCacheSize(104857600)
                    .threadPoolSize(5)
                    .build();
            imageLoader.isInited();
            imageLoader.init(config);
        }
    }

    private void initCicerone() {
        cicerone = Cicerone.create();
    }

    public static NavigatorHolder getNavigatorHolder() {
        return cicerone.getNavigatorHolder();
    }

    public static Router getRouter() {
        return cicerone.getRouter();
    }
}
