package app.android.chem.producthunt.utils;

import android.support.annotation.NonNull;

import com.orhanobut.hawk.Hawk;

import app.android.chem.producthunt.BuildConfig;

/**
 * Created by chem on 05.09.17.
 */

public final class PreferenceUtils {

    public static final String ACCESS_TOKEN = "access_token";

    public static void saveToken(@NonNull String accessToken){
        Hawk.put(ACCESS_TOKEN, accessToken);
    }

    public static String getAccessToken() {
        return Hawk.get(ACCESS_TOKEN, BuildConfig.ACCESS_TOKEN);
    }

}
