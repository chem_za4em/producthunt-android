package app.android.chem.producthunt.api;

import app.android.chem.producthunt.model.CategoriesList;
import app.android.chem.producthunt.model.PostsList;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by chem on 05.09.17.
 */

public interface ProductHuntService {

    @GET("me/feed")
    Observable<PostsList> getTechPosts(@Query("page") int page);

    @GET("categories/{category}/posts")
    Observable<PostsList> getPostsByCategory(@Path("category") String category,
                                              @Query("days_ago") int page);

    @GET("categories")
    Observable<CategoriesList> getCategories();

}
