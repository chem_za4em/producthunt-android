package app.android.chem.producthunt.presentation.view;

import java.util.List;

public interface CategoriesView {

    void showProgress();
    void dismissProgress();
    void showData(List<String> categories);
    void showError();

}
