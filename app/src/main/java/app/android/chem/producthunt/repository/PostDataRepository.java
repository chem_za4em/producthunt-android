package app.android.chem.producthunt.repository;

import java.util.List;

import app.android.chem.producthunt.api.ApiFactory;
import app.android.chem.producthunt.helper.AsyncTransformer;
import app.android.chem.producthunt.model.Category;
import app.android.chem.producthunt.model.Post;
import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by chem on 05.09.17.
 */

public class PostDataRepository implements PostRepository {

    @Override
    public Observable<List<Post>> getTechPosts(int page) {
        return ApiFactory.getReminderService().getTechPosts(page)
                .flatMap(postsList -> {
                    return Observable.just(postsList.getPosts());
                })
                .compose(new AsyncTransformer<>());
    }

    @Override
    public Observable<List<Post>> getPostsByCategory(String category, int page) {
        return ApiFactory.getReminderService().getPostsByCategory(category, page)
                .flatMap(postsList -> {
                    return Observable.just(postsList.getPosts());
                })
                .compose(new AsyncTransformer<>());
    }

    @Override
    public Observable<List<Category>> getCategories() {
        return ApiFactory.getReminderService().getCategories()
                .flatMap(categoriesList -> {
                    return Observable.just(categoriesList.getCategories());
                })
                .flatMap(categories -> {
                    Observable.just(categories);
                    Realm.getDefaultInstance().executeTransaction(realm -> {
                        realm.delete(Category.class);
                        realm.insert(categories);
                    });
                    return Observable.just(categories);
                })
                .onErrorResumeNext(throwable -> {
                    Realm realm = Realm.getDefaultInstance();
                    RealmResults<Category> categories = realm.where(Category.class).findAll();
                    return Observable.just(realm.copyFromRealm(categories));
                })
                .compose(new AsyncTransformer<>());
    }
}
