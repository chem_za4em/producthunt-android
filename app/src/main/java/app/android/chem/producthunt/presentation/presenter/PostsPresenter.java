package app.android.chem.producthunt.presentation.presenter;


import java.util.ArrayList;
import java.util.List;

import app.android.chem.producthunt.model.Post;
import app.android.chem.producthunt.presentation.view.PostsView;
import app.android.chem.producthunt.repository.RepositoryProvider;
import app.android.chem.producthunt.utils.Screens;
import ru.terrakok.cicerone.Router;

public class PostsPresenter{

    private Router router;
    private String category;
    private List<Post> posts;
    private int page;
    private int lastVisibleItem;
    private boolean viewInited;
    private PostsView view;

    public PostsPresenter(Router router, String category, PostsView view){
        this.router = router;
        this.category= category;
        this.view = view;
        posts = new ArrayList<>();
        page = 0;
    }

    public void onSettingsClicked(){
        router.navigateTo(Screens.CATEGORIES_SCREEN);
    }

    public void onViewInited(){
        if (!viewInited) {
            RepositoryProvider.getPostRepository().getPostsByCategory(category.toLowerCase(), page)
                    .doOnSubscribe(disposable -> view.showProgress())
                    .doOnTerminate(view::dismissProgress)
                    .subscribe(posts -> {
                        this.posts = posts;
                        if (posts.isEmpty())
                            view.showNotFound();
                        else {
                            view.showData(posts);
                            page++;
                        }
                    }, throwable -> view.showErrors());
        viewInited = true;
        }
    }

    public void onRefreshSwipe(){
        page = 0;
        lastVisibleItem = 0;
        RepositoryProvider.getPostRepository().getPostsByCategory(category.toLowerCase(), page)
                .doOnSubscribe(disposable -> view.removeData())
                .doOnTerminate(view::dismissUpdateProgress)
                .subscribe(posts -> {
                    this.posts = posts;
                    if (posts.isEmpty())
                        view.showNotFound();
                    else {
                        view.showData(posts);
                        page++;
                    }
                }, throwable -> view.showErrors());
    }

    public void onListViewScrolled(int firstVisibleItem, int totalItemCount){

        if (totalItemCount > 0)
            if (firstVisibleItem == totalItemCount - 6 && firstVisibleItem > lastVisibleItem) {
                lastVisibleItem = firstVisibleItem;
                RepositoryProvider.getPostRepository()
                        .getPostsByCategory(category.toLowerCase(), page)
                        .subscribe(posts -> {
                            this.posts.addAll(posts);
                            view.showData(posts);
                            if (!posts.isEmpty()) page++;
                        });
            }

    }

    public void onPostClicked(int i){

//        if (posts.size()-1 < i)
//            return;
        router.navigateTo(Screens.DETAILS_SCREEN, posts.get(i));
    }
}
