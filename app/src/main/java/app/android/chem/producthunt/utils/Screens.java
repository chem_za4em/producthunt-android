package app.android.chem.producthunt.utils;

/**
 * Created by chem on 05.09.17.
 */

public class Screens {
    public static final String POST_SCREEN = "posts screen";
    public static final String CATEGORIES_SCREEN = "categories screen";
    public static final String DETAILS_SCREEN = "details screen";

}
