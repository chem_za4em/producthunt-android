package app.android.chem.producthunt.presentation.presenter;


import app.android.chem.producthunt.presentation.view.NavigationActivityView;
import app.android.chem.producthunt.utils.Constants;
import app.android.chem.producthunt.utils.Screens;
import ru.terrakok.cicerone.Router;


public class NavigationActivityPresenter{

    private Router router;
    private NavigationActivityView view;

    public NavigationActivityPresenter(Router router, NavigationActivityView view){
        this.view = view;
        this.router = router;
        router.newRootScreen(Screens.POST_SCREEN, Constants.defCategory);
    }


    public void onBackPressed() {
        router.exit();
    }

}
