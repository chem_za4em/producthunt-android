package app.android.chem.producthunt.repository;

/**
 * Created by chem on 05.09.17.
 */

public class RepositoryProvider {

    private static PostRepository postRepository;

    private RepositoryProvider(){
    }

    public static PostRepository getPostRepository(){
        if (postRepository == null)
            postRepository = new PostDataRepository();
        return postRepository;
    }


    public static void setPostRepository(PostRepository postRepository) {
        RepositoryProvider.postRepository = postRepository;
    }
}
