package app.android.chem.producthunt.repository;

import java.util.List;

import app.android.chem.producthunt.model.Category;
import app.android.chem.producthunt.model.Post;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by chem on 05.09.17.
 */

public interface PostRepository {


    Observable<List<Post>> getTechPosts(int page);

    Observable<List<Post>> getPostsByCategory(String category, int page);

    Observable<List<Category>> getCategories();

}
