package app.android.chem.producthunt.api;

import java.io.IOException;

import app.android.chem.producthunt.utils.PreferenceUtils;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by chem on 05.09.17.
 */

public class TokenInterceptor implements Interceptor {

    private final String accessToken;

    private TokenInterceptor(){
        accessToken = PreferenceUtils.getAccessToken();
    }

    public static Interceptor create(){
        return new TokenInterceptor();
    }

    @Override public Response intercept(Chain chain) throws IOException {
        Request request = chain.request().newBuilder()
                .header("Authorization", "Bearer "+
                        accessToken).build();
        return chain.proceed(request);
    }
}
