package app.android.chem.producthunt.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import app.android.chem.producthunt.ProductHuntApp;
import app.android.chem.producthunt.R;
import app.android.chem.producthunt.adapter.PostsListAdapter;
import app.android.chem.producthunt.model.Post;
import app.android.chem.producthunt.presentation.presenter.PostsPresenter;
import app.android.chem.producthunt.presentation.view.PostsView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PostsFragment extends Fragment implements PostsView, AbsListView.OnScrollListener,
        AdapterView.OnItemClickListener{

    public static final String TAG = "PostsFragment";

    PostsPresenter mPostsPresenter;
    @BindView(R.id.posts_list)
    ListView postsList;
    @BindView(R.id.posts_swiperefresh)
    SwipeRefreshLayout postsSwiperefresh;
    @BindView(R.id.posts_progress)
    ProgressBar postsProgress;
    Unbinder unbinder;
    @BindView(R.id.posts_error)
    TextView postsError;
    @BindView(R.id.posts_not_found)
    TextView postsNotFound;


    public interface OnPostClicked {
        void onPostClicked(int posistion);
    }

    private String currentCategory;
    private OnPostClicked onPostClicked;
    private PostsListAdapter postsListAdapter;


    public static PostsFragment newInstance(String category) {
        PostsFragment fragment = new PostsFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.currentCategory = category;
        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_posts, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPostsPresenter = new PostsPresenter(ProductHuntApp.getRouter(), currentCategory, this);

        initViews();
    }

    private void initViews() {
        onPostClicked = posistion -> {

        };
        postsListAdapter = new PostsListAdapter(onPostClicked, getActivity());
        postsList.setAdapter(postsListAdapter);
        postsList.setOnScrollListener(this);
        postsList.setOnItemClickListener(this);
        mPostsPresenter.onViewInited();
        postsSwiperefresh.setOnRefreshListener(() -> {
            mPostsPresenter.onRefreshSwipe();
        });
    }

    @Override
    public void showProgress() {
        postsProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgress() {
        postsProgress.setVisibility(View.GONE);
    }

    @Override
    public void showErrors() {
        postsError.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNotFound() {
        postsNotFound.setVisibility(View.VISIBLE);
    }

    @Override
    public void showData(List<Post> posts) {
        postsError.setVisibility(View.GONE);
        postsNotFound.setVisibility(View.GONE);
        postsListAdapter.addPosts(posts);
    }

    @Override
    public void removeData() {
        postsListAdapter.removePosts();
    }

    @Override
    public void dismissUpdateProgress() {
        postsSwiperefresh.setRefreshing(false);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        mPostsPresenter.onPostClicked(i);
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem,
                         int visibleItemCount, int totalItemCount) {
        mPostsPresenter.onListViewScrolled(firstVisibleItem, totalItemCount);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.category, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.category_settings) {
            mPostsPresenter.onSettingsClicked();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
