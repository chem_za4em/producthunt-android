package app.android.chem.producthunt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import app.android.chem.producthunt.ProductHuntApp;
import app.android.chem.producthunt.R;
import app.android.chem.producthunt.model.Post;
import app.android.chem.producthunt.ui.fragment.PostsFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by chem on 06.09.17.
 */

public class PostsListAdapter extends BaseAdapter {

    PostsFragment.OnPostClicked onPostClicked;
    List<Post> posts;
    Context context;
    private ViewHolder viewHolder;
    private ImageLoader imageLoader;


    public PostsListAdapter(PostsFragment.OnPostClicked onPostClicked, Context context) {
        this.onPostClicked = onPostClicked;
        this.context = context;
        posts = new ArrayList<>();
        imageLoader = ImageLoader.getInstance();
    }

    public void addPosts(List<Post> posts) {
        this.posts.addAll(posts);
        notifyDataSetChanged();
    }

    public void removePosts(){
        posts.clear();
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return posts.size();
    }

    @Override
    public Object getItem(int i) {
        return posts.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.post_item, viewGroup, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        initViews(posts.get(i));
//        viewHolder.postLayout.setOnClickListener(v -> {
//            onPostClicked.onPostClicked(i);
//        });

        return view;
    }

    private void initViews(Post post) {
        viewHolder.productName.setText(post.getName());
        viewHolder.productDiscription.setText(post.getTagline());
        viewHolder.upvotes.setText(String.valueOf(post.getVotesCount()));
        imageLoader.displayImage(post.getThumbnail().getImageUrl(), viewHolder.thumbnail,
                ProductHuntApp.imgDisplayOptions);
    }

    static class ViewHolder {
        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.product_discription)
        TextView productDiscription;
        @BindView(R.id.up_votes)
        TextView upvotes;
        @BindView(R.id.post_layout)
        LinearLayout postLayout;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
