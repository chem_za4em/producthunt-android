package app.android.chem.producthunt.presentation.presenter;


import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.util.ArrayList;
import java.util.List;

import app.android.chem.producthunt.model.Category;
import app.android.chem.producthunt.presentation.view.CategoriesView;
import app.android.chem.producthunt.repository.RepositoryProvider;
import app.android.chem.producthunt.utils.Screens;
import ru.terrakok.cicerone.Router;


public class CategoriesPresenter{

    private Router router;
    CategoriesView view;

    public CategoriesPresenter(Router router, CategoriesView view){
        this.view = view;
        this.router = router;
        categories = new ArrayList<>();
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<String> getCategories() {
        return categories;
    }

    private List<String> categories;



    public void onViewInited(){

        RepositoryProvider.getPostRepository().getCategories()
                .doOnSubscribe(disposable -> view.showProgress())
                .doOnTerminate(view::dismissProgress)
                .flatMapIterable(categories -> categories)
                .map(Category::getName)
                .toList()
                .subscribe(categories -> { view.showData(categories);
                                setCategories(categories);
                },
                        throwable -> {
                            Log.d("category", "onViewInited: " + ((HttpException) throwable).code());
                            view.showError();
                });
    }

    public void onCategoryClicked(int i){
        router.newRootScreen(Screens.POST_SCREEN, categories.get(i));
    }

}
