package app.android.chem.producthunt.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import app.android.chem.producthunt.ProductHuntApp;
import app.android.chem.producthunt.R;
import app.android.chem.producthunt.model.Post;
import app.android.chem.producthunt.presentation.presenter.NavigationActivityPresenter;
import app.android.chem.producthunt.presentation.view.NavigationActivityView;
import app.android.chem.producthunt.ui.fragment.CategoriesFragment;
import app.android.chem.producthunt.ui.fragment.DetailsFragment;
import app.android.chem.producthunt.ui.fragment.PostsFragment;
import app.android.chem.producthunt.utils.Screens;
import butterknife.BindView;
import butterknife.ButterKnife;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.android.SupportAppNavigator;

public class NavigationActivity extends AppCompatActivity implements NavigationActivityView {

    public static final String TAG = "NavigationActivity";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private String currentCategory;



    NavigationActivityPresenter mNavigationActivityPresenter;

    private Navigator navigator = new SupportAppNavigator(this,
            R.id.fragment_container) {


        @Override
        protected Intent createActivityIntent(String screenKey, Object data) {
            return null;
        }


        @Override
        protected android.support.v4.app.Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case Screens.POST_SCREEN:
                    currentCategory = data.toString();
                    toolbar.setTitle(currentCategory);
                    return  PostsFragment.newInstance(currentCategory);

                case Screens.CATEGORIES_SCREEN:
                    toolbar.setTitle(R.string.categories);
                    return CategoriesFragment.newInstance();

                case Screens.DETAILS_SCREEN:
                    toolbar.setTitle(R.string.post);
                    return DetailsFragment.newInstance((Post) data);

                default:
                    return null;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        ButterKnife.bind(this);
        mNavigationActivityPresenter = new NavigationActivityPresenter(ProductHuntApp.getRouter(),
                this);
        initViews();

    }

    private void initViews() {
        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.tech));
    }


    @Override
    protected void onResume() {
        super.onResume();
        ProductHuntApp.getNavigatorHolder().setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        ProductHuntApp.getNavigatorHolder().removeNavigator();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        toolbar.setTitle(currentCategory);
        mNavigationActivityPresenter.onBackPressed();
    }
}
