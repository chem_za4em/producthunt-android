package app.android.chem.producthunt.presentation.view;

import java.util.List;

import app.android.chem.producthunt.model.Post;

public interface PostsView{

    void showProgress();
    void dismissProgress();
    void showErrors();
    void showNotFound();
    void showData(List<Post> posts);
    void removeData();
    void dismissUpdateProgress();
}
