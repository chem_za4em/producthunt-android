package app.android.chem.producthunt.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import app.android.chem.producthunt.ProductHuntApp;
import app.android.chem.producthunt.R;
import app.android.chem.producthunt.presentation.presenter.CategoriesPresenter;
import app.android.chem.producthunt.presentation.view.CategoriesView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CategoriesFragment extends Fragment implements CategoriesView {

    public static final String TAG = "CategoriesFragment";


    CategoriesPresenter mCategoriesPresenter;

    @BindView(R.id.category_progress)
    ProgressBar categoryProgress;
    @BindView(R.id.category_error)
    TextView categoryError;


    @BindView(R.id.categories_list)
    ListView categoriesList;
    Unbinder unbinder;
    ArrayAdapter<String> adapter;

    public static CategoriesFragment newInstance() {
        CategoriesFragment fragment = new CategoriesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_categories, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCategoriesPresenter = new CategoriesPresenter(ProductHuntApp.getRouter(), this);
        initViews();
    }

    private void initViews() {
        adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1);
        categoriesList.setAdapter(adapter);
        categoriesList.setOnItemClickListener((adapterView, view, i, l) -> {
            mCategoriesPresenter.onCategoryClicked(i);
        });
        mCategoriesPresenter.onViewInited();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.close, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.category_close) {
            getActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        categoryProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgress() {
        categoryProgress.setVisibility(View.GONE);
    }

    @Override
    public void showData(List<String> categories) {
        categoryError.setVisibility(View.GONE);
        adapter.clear();
        adapter.addAll(categories);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showError() {
        categoryError.setVisibility(View.VISIBLE);
    }

}
