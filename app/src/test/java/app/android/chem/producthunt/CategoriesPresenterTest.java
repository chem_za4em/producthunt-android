package app.android.chem.producthunt;

import android.content.Context;
import android.view.View;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.configuration.DefaultMockitoConfiguration;
import org.mockito.internal.stubbing.defaultanswers.ReturnsEmptyValues;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import app.android.chem.producthunt.presentation.presenter.CategoriesPresenter;
import app.android.chem.producthunt.presentation.view.CategoriesView;
import app.android.chem.producthunt.repository.RepositoryProvider;
import app.android.chem.producthunt.utils.Screens;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import ru.terrakok.cicerone.Router;

/**
 * Created by chem on 17.09.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class CategoriesPresenterTest {

    CategoriesView viewState = Mockito.mock(CategoriesView.class);

    Router router = Mockito.mock(Router.class);
    ArrayList<String> categories = new ArrayList<>();

    CategoriesPresenter presenter;

    public class MockitoConfiguration extends
            DefaultMockitoConfiguration {
        public Answer<Object> getDefaultAnswer() {
            return new ReturnsEmptyValues() {
                @Override
                public Object answer(InvocationOnMock inv) {
                    Class<?> type = inv.getMethod().getReturnType();
                    if (type.isAssignableFrom(Observable.class)) {
                        return Observable.error(createException(inv));
                    } else if (type.isAssignableFrom(Single.class)) {
                        return Single.error(createException(inv));
                    } else {
                        return super.answer(inv);
                    }
                }
            };
        }

        @NonNull
        private RuntimeException createException(
                InvocationOnMock invocation) {
            String s = invocation.toString();
            return new RuntimeException(
                    "No mock defined for invocation " + s);
        }
    }

    @Before
    public void initPresenter() throws Exception{
        presenter = new CategoriesPresenter(router, viewState);
        categories.add("tech");
        categories.add("game");
        presenter.setCategories(categories);
    }

    @After
    public void destroyPresenter() throws Exception{
        presenter = null;
        categories.clear();
    }

    @Test
    public void testCreated() throws Exception{
        Assert.assertNotNull(presenter);
    }

    @Test
    public void viewInited() throws Exception{
        CategoryTestRepository categoryTestRepository = new CategoryTestRepository();
        categoryTestRepository.setSuccess(true);
        RepositoryProvider.setPostRepository(categoryTestRepository);
        presenter.onViewInited();
        Mockito.verify(viewState, Mockito.times(1)).showProgress();
        Mockito.verify(viewState, Mockito.times(1)).dismissProgress();
        Mockito.verify(viewState, Mockito.times(1)).showData(presenter.getCategories());
        Mockito.verifyNoMoreInteractions(viewState);
    }


    @Test
    public void categoryClicked(){
        presenter.onCategoryClicked(0);
        Mockito.verify(router).newRootScreen(Screens.POST_SCREEN, "tech");
        Mockito.verifyNoMoreInteractions(router);
    }

    @Test
    public void categorySecondClicked(){
        presenter.onCategoryClicked(1);
        Mockito.verify(router).newRootScreen(Screens.POST_SCREEN, "game");
        Mockito.verifyNoMoreInteractions(router);
    }



}
