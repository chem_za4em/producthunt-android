package app.android.chem.producthunt;

import java.util.List;

import app.android.chem.producthunt.model.Category;
import app.android.chem.producthunt.model.Post;
import app.android.chem.producthunt.repository.PostRepository;
import io.reactivex.Observable;

/**
 * Created by chem on 17.09.17.
 */

public class TestDataRepository implements PostRepository {


    @Override
    public Observable<List<Post>> getTechPosts(int page) {
        return null;
    }

    @Override
    public Observable<List<Post>> getPostsByCategory(String category, int page) {
        return null;
    }

    @Override
    public Observable<List<Category>> getCategories() {
        return null;
    }
}
