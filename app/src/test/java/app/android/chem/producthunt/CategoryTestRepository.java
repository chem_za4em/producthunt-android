package app.android.chem.producthunt;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import app.android.chem.producthunt.model.Category;
import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by chem on 17.09.17.
 */

public class CategoryTestRepository extends TestDataRepository {

    private boolean success;

    @Override
    public Observable<List<Category>> getCategories() {
        if (success) {
            List<Category> categories = new ArrayList<>();
            Category category = new Category();
            category.setName("tech");
            categories.add(category);
            return Observable.just(categories);
        }
        else return Observable.error(new IOException());
    }


    public void setSuccess(boolean success) {
        this.success = success;
    }
}
